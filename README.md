# The Shape of Necessity

LINK is collaborating on this project with the Master in Design for Emergent Futures and Fab Lab Barcelona to document emergent practices, curate a collection of creative solutions to create a book portraying new paradigms of necessity in emergency. 